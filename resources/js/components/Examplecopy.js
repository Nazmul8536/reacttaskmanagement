import React from 'react';
import ReactDOM from 'react-dom';

function Examplecopy() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                       <h2>hello</h2>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Examplecopy;

if (document.getElementById('examplecopy')) {
    ReactDOM.render(<Examplecopy />, document.getElementById('examplecopy'));
}
